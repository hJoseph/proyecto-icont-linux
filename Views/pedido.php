<?php

?>

<section class="panel">

    <header class="panel-heading" align="center">
        PRODUCTOS SOLICITADOS
    </header>

    <table class="table table-striped">
        <thead>
        <tr>
            <td width="20">Imagen</td>
            <td>Producto</td>
            <td>Cant.</td>
            <td>Precio</td>
            <td>Total</td>
            <td>Tipo</td>
            <td>Opcion</td>
        </tr>

        <?php
        foreach ($showPreVenta as $preventa) {
        ?>

        <tr>
            <td><img src="<?PHP echo url();
                echo '/Views/';
                echo $preventa['imagen'] ?>" width="60" height="60"></td>
            <td><b><?php echo $preventa['producto']; ?></b></td>
            <td><?php echo $preventa['cantidad']; ?></td>
            <td><?php echo $preventa['precio']; ?></td>
            <td><?php echo $preventa['totalPrecio']; ?></td>
            <td><?php echo $preventa['tipo']; ?></td>
            <td>
                <?PHP
                echo "<a style=\"cursor:pointer;\"  class='btn btn-success'   
                               onclick=\"editarPreventa('".$preventa['idproducto']."','".$preventa['tipo']."')\">
                               <i class='icon_pencil-edit'></i></a>";

                echo "<a style=\"cursor:pointer;\"  class='btn btn-danger'
                         onclick=\"deleteDatoP('" . $preventa['idproducto'] . "')\">
                <i class='icon_minus-box'></i></a>"; ?>
            </td>
         <?php  }  ?>
        </tr>

        <tr>
            <td colspan="3"></td>
            <td> Total :</td>
            <td>
                <b>
                    <h2>
                        <?php foreach ($totalPreVenta as $totalVenta) {
                            echo $totalVenta['total'];
                        } ?>
                    </h2>
                </b>
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <a data-toggle="modal" class="btn btn-primary enabled" href="Factura.php?usuario=<?php echo $usuario; ?>&password=<?php echo $contrasena;?>" data-target="#myModal">
                    <strong> ACEPTAR</strong></a>
                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        </div><!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            </td>
            <td colspan="3" align="rigth">
                <?PHP
                if (isset($preventa['idUser'])) {
                      echo "<a style=\"cursor:pointer;\"  class='btn btn-danger'
                         onclick=\"deleteAllPreventa('".$preventa['idUser']."')\">
                      <i class='icon_minus-box'></i><strong> CANCELAR</strong></a>";
                 }
                 ?>
            </td>
        </tr>

        </thead>
           <div id="formulario" style="display:none;">
        </div>
    </table>
    </div>
    <!-- /.table-responsive -->
    </div>
    <!--main content end-->
</section>