<!DOCTYPE html>
<html lang="en">
<head>
    <?php  require "../Model/ModelUrl.php";  ?>

    <meta charset="utf-8">
    <?php include("head.php"); ?>


    <script language="javascript" type="text/javascript">
        //*** Codigo para Validar que sea un campo de Letras
        function soloLetras(e) {
            key = e.keyCode || e.which;
            tecla = String.fromCharCode(key).toLowerCase();
            letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
            especiales = "8-37-39-46";

            tecla_especial = false
            for (var i in especiales) {
                if (key == especiales[i]) {
                    tecla_especial = true;
                    break;
                }
            }

            if (letras.indexOf(tecla) == -1 && !tecla_especial) {
                return false;
            }
        }
    </script>

    <script language="javascript" type="text/javascript">
        //*** Este Codigo permite Validar que sea un campo Numerico
        function Solo_Numerico(variable) {
            Numer = parseInt(variable);
            if (isNaN(Numer)) {
                return "";
            }
            return Numer;
        }

        function ValNumero(Control) {
            Control.value = Solo_Numerico(Control.value);
        }
    </script>
    <?PHP include("DropDown.php"); ?>
</head>

<body>
<?PHP
//// vamos a activar el menu
//include("php_conexion.php");
//$sql = "UPDATE `menu` SET `estado` = 'Activo' WHERE `menu`.`idmenu` ='2' ";
//$result = mysql_query($sql, $conn) or die(mysql_error());
//
//// inhabilitar los otros
//
//include("php_conexion.php");
//$sql = "UPDATE `menu` SET `estado` = 'NoActivo' WHERE `idmenu`<>'2' ";
//$result = mysql_query($sql, $conn) or die(mysql_error());
//

?>

<!-- container section start -->
<section id="container" class="">
    <!--header start-->
    <header class="header dark-bg">
        <div class="toggle-nav">
            <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                    class="icon_menu"></i></div>
        </div>

        <!--logo start-->
        <?PHP include("logo.php"); ?>
        <!--logo end-->

        <div class="nav search-row" id="top_menu">
            <!--  search form start -->
            <ul class="nav top-menu">
                <li>
                    <form class="navbar-form">
                        <input class="form-control" placeholder="Search" type="text">
                    </form>
                </li>
            </ul>
            <!--  search form end -->
        </div>

        <div class="top-nav notification-row">
            <!-- notificatoin dropdown start-->
            <ul class="nav pull-right top-menu">

                <!-- task notificatoin start -->
                <li id="task_notificatoin_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="icon-task-l"></i>
                                <span class="badge bg-important">5</span>
                    </a>
                    <ul class="dropdown-menu extended tasks-bar">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li>
                            <p class="blue">You have 5 pending tasks</p>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">Design PSD</div>
                                    <div class="percent">90%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                        <span class="sr-only">90% Complete (success)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">
                                        Project 1
                                    </div>
                                    <div class="percent">30%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="30"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 30%">
                                        <span class="sr-only">30% Complete (warning)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">Digital Marketing</div>
                                    <div class="percent">80%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="80"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                        <span class="sr-only">80% Complete</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">Logo Designing</div>
                                    <div class="percent">78%</div>
                                </div>
                                <div class="progress progress-striped">
                                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="78"
                                         aria-valuemin="0" aria-valuemax="100" style="width: 78%">
                                        <span class="sr-only">78% Complete (danger)</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="task-info">
                                    <div class="desc">Mobile App</div>
                                    <div class="percent">50%</div>
                                </div>
                                <div class="progress progress-striped active">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0"
                                         aria-valuemax="100" style="width: 50%">
                                        <span class="sr-only">50% Complete</span>
                                    </div>
                                </div>

                            </a>
                        </li>
                        <li class="external">
                            <a href="#">See All Tasks</a>
                        </li>
                    </ul>
                </li>
                <!-- task notificatoin end -->
                <!-- inbox notificatoin start-->
                <li id="mail_notificatoin_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <i class="icon-envelope-l"></i>
                        <span class="badge bg-important">5</span>
                    </a>
                    <ul class="dropdown-menu extended inbox">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li>
                            <p class="blue">You have 5 new messages</p>
                        </li>
                        <li>
                            <a href="#">
                                <span class="photo"><img alt="avatar" src="./img/avatar-mini.jpg"></span>
                                <span class="subject">
                                    <span class="from">Greg  Martin</span>
                                    <span class="time">1 min</span>
                                    </span>
                                <span class="message">
                                        I really like this admin panel.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="photo"><img alt="avatar" src="./img/avatar-mini2.jpg"></span>
                                <span class="subject">
                                    <span class="from">Bob   Mckenzie</span>
                                    <span class="time">5 mins</span>
                                    </span>
                                <span class="message">
                                     Hi, What is next project plan?
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="photo"><img alt="avatar" src="./img/avatar-mini3.jpg"></span>
                                <span class="subject">
                                    <span class="from">Phillip   Park</span>
                                    <span class="time">2 hrs</span>
                                    </span>
                                <span class="message">
                                        I am like to buy this Admin Template.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="photo"><img alt="avatar" src="./img/avatar-mini4.jpg"></span>
                                <span class="subject">
                                    <span class="from">Ray   Munoz</span>
                                    <span class="time">1 day</span>
                                    </span>
                                <span class="message">
                                        Icon fonts are great.
                                    </span>
                            </a>
                        </li>
                        <li>
                            <a href="#">See all messages</a>
                        </li>
                    </ul>
                </li>
                <!-- inbox notificatoin end -->
                <!-- alert notification start-->
                <li id="alert_notificatoin_bar" class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                        <i class="icon-bell-l"></i>
                        <span class="badge bg-important">7</span>
                    </a>
                    <ul class="dropdown-menu extended notification">
                        <div class="notify-arrow notify-arrow-blue"></div>
                        <li>
                            <p class="blue">You have 4 new notifications</p>
                        </li>
                        <li>
                            <a href="#">
                                <span class="label label-primary"><i class="icon_profile"></i></span>
                                Friend Request
                                <span class="small italic pull-right">5 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="label label-warning"><i class="icon_pin"></i></span>
                                John location.
                                <span class="small italic pull-right">50 mins</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="label label-danger"><i class="icon_book_alt"></i></span>
                                Project 3 Completed.
                                <span class="small italic pull-right">1 hr</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="label label-success"><i class="icon_like"></i></span>
                                Mick appreciated your work.
                                <span class="small italic pull-right"> Today</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">See all notifications</a>
                        </li>
                    </ul>
                </li>


                <!-- alert notification end-->
                <!-- user login dropdown start-->
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="profile-ava">
                                <img src="<?PHP
                                //                                echo $foto;
                                //
                                ?>" alt="Usuario" height="35" width="35">


                            </span>
                        <span class="username"><?PHP
                            //                            echo $nombres;
                            ?> </span>
                        <b class="caret"></b>
                    </a>
                    <?PHP include("menuSalida.php"); ?>
        </div>
    </header>
    <!--header end-->
    <?PHP include("menu.php"); ?>
    </div>
    </aside>
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-edit"></i><strong> REGISTROS DEL SISTEMA </strong></h3>
                    <div class="<?php echo $alerta;?>" role="alert">
                       <b><?php echo $messageAlerta;?> </b>
                    </div>
                    <ol class="breadcrumb">
                        <?PHP include("menuOpciones.php"); ?>
                    </ol>
                </div>
            </div>

            <!--modal start-->

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <section class="panel">

                            <header class="panel-heading">
                                Usuarios
                            </header>
                            <header class="panel-heading">
                                <div class="panel-body">
                                    <div align="right">
                                        <button href="#add" title="" data-placement="left" data-toggle="modal"
                                                class="btn btn-primary tooltips" type="button"
                                                data-original-title="Nuevo Usuario"><span class="fa fa-plus"></span> AGREGAR NUEVO USUARIO
                                        </button>
                                    </div>

                                    <div id="add" class="modal fade" tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel" aria-hidden="true">
                                        <form class="form-validate form-horizontal" name="form2" action="Registros.php" method="post">
                                            <input name="usuarioLogin" value="<?php echo $usuario;?>" type="hidden" >
                                            <input name="passwordLogin" value="<?php echo $password;?>" type="hidden" >
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-hidden="true">x
                                                        </button>
                                                        <h3 id="myModalLabel" align="center">Registrar Nuevo
                                                            Usuario</h3>
                                                    </div>
                                                    <div class="modal-body">
                                                        <label for="proveedor"
                                                               class="control-label col-lg-2">Nombre:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="nombre"  name="nombre" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br>
                                                        <label for="responsable"
                                                               class="control-label col-lg-2">Tipo:</label>
                                                        <div class="col-lg-10">
                                                            <select class="form-control input-lg m-bot15" name="tipo">
                                                                <option value="ADMINISTRADOR">ADMINISTRADOR</option>
                                                                <option value="VENTAS">VENTAS</option>

                                                            </select>
                                                            <input class="form-control input-lg m-bot15" id="login"   name="DDDD" minlength="5" type="hidden"/>
                                                        </div>
                                                        <label for="responsable"
                                                               class="control-label col-lg-2">Login:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="login" name="login" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br>
                                                        <label for="responsable" class="control-label col-lg-2">Password:</label>
                                                        <div class="col-lg-10">
                                                            <input class="form-control input-lg m-bot15" id="password"
                                                                   name="password" minlength="5" type="text" required/>
                                                        </div>
                                                        <br><br><br>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button class="btn btn-danger" data-dismiss="modal"  aria-hidden="true"><strong>Cerrar</strong></button>

                                                        <button name="a_nuevo" type="submit" class="btn btn-primary"><strong>Registrar</strong></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </header>
                            <!-- /.panel-heading -->
                            <div class="panel-body">
                                <div class="dataTable_wrapper">
                                    <table class="table table-striped table-bordered table-hover"
                                           id="dataTables-example">
                                        <thead>
                                        <tr>
                                            <th><i class="icon_images"></i> FOTO</th>
                                            <th><i class="icon_contacts"></i> NOMBRE</th>
                                            <th><i class="icon_folder"></i> TIPO</th>
                                            <th><i class="icon_contacts_alt"></i> LOGIN</th>
                                            <th><i class="icon_key"></i> PASSWORD</th>
                                            <th><i class="icon_cog"></i> ACCIONES</th>

                                        </tr>
                                        </thead>
                                        <?php
                                        while ($datosUsuario = mysqli_fetch_array($allUsuarios)) {  ?>

                                           <tr>
                                                <td><img src="<?PHP echo url(); echo '/Views/'; echo $datosUsuario['foto']; ?>"  width="50" height="50"> </td>
                                                <td><?php echo $datosUsuario['nombre']; ?></td>
                                                <td><?php echo $datosUsuario['tipo']; ?></td>
                                                <td><?php echo $datosUsuario['login']; ?></td>
                                                <td><?php echo $datosUsuario['password']; ?></td>
                                                <td>
                                                    <a href="#a<?php echo $datosUsuario[0]; ?>" role="button" class="btn btn-success" data-toggle="modal"><i  class="icon_check_alt2"></i></a>
                                                    <a href="Registros.php?idborrar=<?php echo $datosUsuario[0]; ?>&usuarioLogin=<?php echo $usuario;?>&passwordLogin=<?php echo $password;?>" class="btn btn-danger"><i class="icon_close_alt2"></i></a>
                                                </td>
                                            </tr>
                                            <div id="a<?php echo $datosUsuario[0]; ?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                                <form class="form-validate form-horizontal" name="form2" action="Registros.php" method="post">
                                                    <input name="usuarioLogin" value="<?php echo $usuario;?>" type="hidden" >
                                                    <input name="passwordLogin" value="<?php echo $password;?>" type="hidden" >
                                                    <input type="hidden" name="idUsuario"  value="<?php echo $datosUsuario['id_usu']; ?>">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×
                                                                </button>
                                                                <h3 id="myModalLabel" align="center">Cambiar Informacion del Usuario</h3>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="form-group ">
                                                                    <label for="proveedor" class="control-label col-lg-2">Nombre:</label>
                                                                    <div class="col-lg-10">
                                                                        <input class="form-control input-lg m-bot15" type="text" name="nombre"  value="<?php echo $datosUsuario['nombre']; ?>">
                                                                        <input type="hidden" name="idUsuario"  value="<?php echo $datosUsuario['id_usu']; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="responsable"
                                                                           class="control-label col-lg-2">Tipo:</label>
                                                                    <div class="col-lg-10">
                                                                        <select class="form-control input-lg m-bot15" name="tipo">
                                                                            <option value="<?php echo $datosUsuario['tipo']; ?>"><?php echo $datosUsuario['tipo']; ?></option>
                                                                            <option value="ADMINISTRADOR"> ADMINISTRADOR </option>
                                                                            <option value="VENTAS">VENTAS</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="direccion" class="control-label col-lg-2">Login:</label>
                                                                    <div class="col-lg-10">
                                                                        <input class="form-control input-lg m-bot15"  type="text" name="login"  value="<?php echo $datosUsuario['login']; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="form-group ">
                                                                    <label for="telefono"
                                                                           class="control-label col-lg-2">Password:</label>
                                                                    <div class="col-lg-10">
                                                                        <input class="form-control input-lg m-bot15"  type="text" name="password"  value="<?php echo $datosUsuario['password']; ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button class="btn btn-default" data-dismiss="modal" aria-hidden="true"><strong>Cerrar</strong> </button>
                                                                    <button name="update_data" type="submit"  class="btn btn-primary"><strong>Actualizar Datos</strong></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        <?php } ?>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>

                        </section>
                    </div>
                </div>



                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                        </section>
                    </div>
                </div>
            </div>


            <!---------- final aqui------->


        </section>
    </section>

    <!--main content end-->
</section>


<!-- container section end -->
<!-- javascripts -->
<script src="<?php echo url(); ?>/Views/js/jquery.js"></script>
<script src="<?php echo url(); ?>/Views/js/bootstrap.min.js"></script>
<!-- nicescroll -->
<script src="<?php echo url(); ?>/Views/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/jquery.nicescroll.js" type="text/javascript"></script>
<!--custome script for all page-->
<script src="<?php echo url(); ?>/Views/js/scripts.js"></script>

<!-- DataTables JavaScript -->
<script src="<?php echo url(); ?>/Views/js/jquery.dataTables.min.js"></script>
<script src="<?php echo url(); ?>/Views/js/dataTables.bootstrap.min.js"></script>

<script>
    $(document).ready(function () {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
</script>


</body>
</html>