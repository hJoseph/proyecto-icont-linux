<?php

class conexion
{

    private $user;
    private $password;
    private $server;
    private $database;
    private $con;

    public function __construct()
    {
        $user = 'root';
        $server = "localhost";
        $database = "icontcarlitos";
        $password = '';
        $this->con = new mysqli($server, $user, $password, $database);
    }


    public function getAllUsers()
    {
        $query = $this->con->query('SELECT nombre, password,tipo,foto,login FROM usuarios');

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }

        return $retorno;
    }

    public function getAllUsersData()
    {
        $query = $this->con->query('SELECT * FROM usuarios');
        return $query;
    }

    public function getAllProveedor()
    {
        $query = $this->con->query('SELECT * FROM proveedor');
        return $query;
    }

    public function getTipoMensajeAlerta()
    {
        $query = $this->con->query('SELECT * FROM `alerta`');
        return $query;
    }

    public function getMensajeAlerta()
    {
        $query = $this->con->query('SELECT * FROM `mensajeAlerta`');
        return $query;
    }

    public function updateMensajeAlerta($mensaje)
    {
        $query = $this->con->query("UPDATE `mensajeAlerta` SET `mensaje` ='".$mensaje." ' WHERE `mensajeAlerta`.`mensajeAlertaId` = 1");
        return $query;
    }

    public function updateAlerta($alerta)
    {
        $query = $this->con->query("UPDATE `alerta` SET `tipoAlerta` ='".$alerta." ' WHERE `alerta`.`alertaId` = 1");
        return $query;
    }
    
    public function getUser($usuario, $contrasena)
    {
        $query = $this->con->query("SELECT * FROM usuarios WHERE login='" . $usuario . "' AND password='" . $contrasena . "'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getUserById($userId)
    {
        $query = $this->con->query(" SELECT * FROM `usuarios` WHERE id_usu='".$userId."' ");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }



    public function getStatus()
    {
        $query = $this->con->query("SELECT * FROM `menu` where idmenu='1'");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getMenu()
    {
        $query = $this->con->query("SELECT * FROM `menu` ");

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getProduct()
    {
        $query = $this->con->query("SELECT * FROM producto");
        return $query;
    }

    public function getPreVenta()
    {
        $query = $this->con->query("SELECT idpreventa,imagen,producto, count(producto) AS cantidad, sum(precio) as totalPrecio, idproducto, pventa, idUser, precio ,tipo
                                           FROM `preventa` GROUP BY producto , idproducto ,tipo ORDER BY idpreventa ASC");
        return $query;
    }

    public function getTotalPreVenta()
    {
        $query = $this->con->query("SELECT SUM( precio ) as total  FROM `preventa`");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }


    public function deleteUnaPreVenta($idproducto){
        $query = $this->con->query("DELETE FROM `preventa` WHERE `preventa`.`idproducto` = $idproducto");
        return $query;
    }

    public function deleteAllPreVenta($idUser){
        $query = $this->con->query("DELETE FROM `preventa` WHERE `preventa`.`idUser` = $idUser");
        return $query;
    }

    public function editPreVenta($idproducto,$tipo){

        $query = $this->con->query("SELECT * FROM preventa WHERE idproducto=$idproducto and tipo='$tipo'" );

        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getCantidad($idproducto,$tipoPedido){

        $query = $this->con->query("SELECT  count( idproducto ) AS cantidad  FROM `preventa`  where idproducto = '$idproducto' and tipo='$tipoPedido'");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function deleteCantidadActual($idproducto,$tipoPedido){
        $query = $this->con->query("DELETE FROM `preventa` WHERE `preventa`.`idProducto` = '$idproducto' and  `preventa`.`tipo` = '$tipoPedido'");
        return $query;
    }

    public function insertNewPreventa($imagen,$productoUpdate,$precio,$idproducto,$pventa,$userId,$tipoPedido){

        $query = $this->con->query("INSERT INTO preventa (`idPreventa`, `imagen`, `producto`, `precio`, `idProducto`, `pventa`, `idUser`, `tipo`)
			                      	VALUES (NULL,'$imagen','$productoUpdate','$precio','$idproducto','$pventa', $userId, '$tipoPedido')");
        return $query;
    }

    public function getPreventaTotal(){

        $query = $this->con->query("SELECT  SUM(precio) as pventa FROM preventa");
        return $query;
    }

    public function getUserSession(){

        $query = $this->con->query("SELECT * FROM `datosUsuarioSesion` limit 1");
        return $query;
    }

    public function getUserPreventa($ci){
        $query = $this->con->query("select * from cliente2 where ci = $ci ");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function registerPreventa($nombre, $nitci, $fecha, $totalApagar, $efectivo, $cambio, $idClientei){

        $query = $this->con->query( "INSERT INTO `clienteDato` (`idCliente`, `nombre`, `ci`, `fecha`, `totalApagar`, `efectivo`, `cambio` , `idClientei`) 
                                           VALUES (NULL, '$nombre', '$nitci', '$fecha', '$totalApagar', '$efectivo', '$cambio', '$idClientei')");
        return $query;
    }

    public function registerNewCi($nombre, $nitci){

        $query = $this->con->query( "INSERT INTO `cliente2` (`idcliente`, `nombre`, `ci`) VALUES (NULL, '$nombre', '$nitci')");
        return $query;
    }

    public function getDataFactura(){

        $query = $this->con->query("SELECT  * from datos ");
        return $query;
    }

    public function getDataCodigoControl(){

        $query = $this->con->query("SELECT  * from codigocontrol ");
        return $query;
    }

    public function getDataFacturaTiquet(){

        $query = $this->con->query("SELECT  * from datosFactura ");
        return $query;
    }

    public function getDataLibroDeVenta(){

        $query = $this->con->query("SELECT  * from librov  order by idlibro DESC LIMIT 1 ");
        return $query;
    }

    public function getClientDataFactura(){

        $query = $this->con->query("SELECT * from clientedato order by idcliente DESC limit 1 ");
        return $query;
    }

    public function getVentaTotalForFactura(){

        $query = $this->con->query("SELECT * from clientetotal limit 1");
        return $query;
    }

    public function getPedidoTotalForFactura(){
        $query = $this->con->query("SELECT idpreventa,imagen,producto,precio, count( idproducto ) AS cantidad, precio*count( idproducto ) as totalPrecio, idproducto, pventa ,tipo FROM `preventa`  GROUP BY idproducto");
        return $query;
    }

    public function getTotalVentaForFactura(){
        $query = $this->con->query("SELECT SUM( precio ) as total  FROM `preventa`");
        return $query;
    }

    public function getVentasDiaCurrent(){
        $query = $this->con->query("select (count(*)+1) as maxid from ventasdia");
        return $query;
    }

    public function registerPreventaDetalle($fecha, $codTransaccion, $nombre, $cantidad, $producto, $precio, $total, $tipo, $usuario, $estadoTransaccion){

        $query = $this->con->query( "INSERT INTO `ventasDetalle` (`ventasDetalleId`, `fecha`, `codTransaccion`, `nombre`, `cantidad`, `producto`, `precio`, `total`, `tipo`, `usuario`, `estadoTransaccion`) 
                                           VALUES (NULL, '$fecha', '$codTransaccion', '$nombre', '$cantidad', '$producto', '$precio', '$total', '$tipo', '$usuario', '$estadoTransaccion')");
        return $query;
    }

    public function getPreVentaTotalByPerson()
    {
        $query = $this->con->query("SELECT sum(precio) as totalPrecio, idUser FROM `preventa` GROUP BY idUser");
        return $query;
    }

    public function registerPreventaDetalleTotal($fecha, $codTransaccion, $nombre, $total, $usuario, $estadoTransaccion){

        $query = $this->con->query( "INSERT INTO `ventasDetalleTotal` (`ventasDetalleId`, `fecha`, `codTransaccion`, `nombre`, `total`, `usuario`, `estadoTransaccion`) 
                                            VALUES (NULL, '$fecha', '$codTransaccion', '$nombre', '$total', '$usuario', '$estadoTransaccion')");
        return $query;
    }


    public function cleanRegisterPreventa(){

        $query = $this->con->query( "truncate `preventa`");
        return $query;
    }

    public function cleanCodigoControl(){

        $query = $this->con->query( "truncate `codigoTransaccion`");
        return $query;
    }

    public function cleanClienteData(){

        $query = $this->con->query( "truncate `clientedato`");
        return $query;
    }
    public function cleanDataFactura(){

        $query = $this->con->query( "truncate `datosFactura`");
        return $query;
    }

    public function getProductoData($idproducto){
        $query = $this->con->query("SELECT  * FROM producto where idproducto='$idproducto'");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function registerPreventaProducto($imagen, $producto, $precio, $idProducto, $pventa, $idUser, $tipo){

        $query = $this->con->query( "INSERT INTO `preventa` (`idPreventa`, `imagen`, `producto`, `precio`, `idProducto`, `pventa`, `idUser`, `tipo`) VALUES
                                           (NULL, '$imagen', '$producto', '$precio', '$idProducto', '$pventa', '$idUser', '$tipo')");
        return $query;
    }


    public function registerDatosFactura($nit, $cambio, $efectivo, $totalApagar, $usuario, $password){

        $query = $this->con->query( "INSERT INTO `datosFactura` (`datosFacturaId`, `nit`, `cambio`, `efectivo`, `totalApagar`, `usuario`, `password`) 
                                            VALUES (NULL, '$nit', '$cambio', '$efectivo', '$totalApagar', '$usuario', '$password');");
        return $query;
    }



    public function registerCodigoControl($codigo){

        $query = $this->con->query( "INSERT INTO `codigoTransaccion` (`idCodigo`, `codigoTransaccion`) VALUES (NULL, '$codigo')");
        return $query;
    }

    public function getCodigoControl()
    {
        $query = $this->con->query("SELECT * FROM `codigoTransaccion`");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function getVentasDetalleTotal()
    {
        $query = $this->con->query("SELECT count(*) as total FROM `ventasDetalleTotal`");
        $retorno = [];

        $i = 0;
        while ($fila = $query->fetch_assoc()) {
            $retorno[$i] = $fila;
            $i++;
        }
        return $retorno;
    }

    public function registerDatosCliente($nombre,$ci,$fecha,$totalApagar,$efectivo,$cambio,$dClientei,$tipoVenta){

        $query = $this->con->query( "INSERT INTO `clientedato` (`idCliente`, `nombre`, `ci`, `fecha`, `totalApagar`, `efectivo`, `cambio`, `idClientei`, `tipoVenta`)
                                                               VALUES (NULL, '$nombre', '$ci', '$fecha', '$totalApagar', '$efectivo', '$cambio', '$dClientei', '$tipoVenta')");
        return $query;
    }

    public function registerNewUser($nombre, $tipo, $login, $password, $destino){

        echo "INSERT INTO `usuarios` (`id_usu`, `login`, `tipo`, `nombre`, `password`, `foto`) VALUES (NULL, '$login', '$tipo', '$nombre', '$password', '$destino')";

        $query = $this->con->query( "INSERT INTO `usuarios` (`id_usu`, `login`, `tipo`, `nombre`, `password`, `foto`) 
                                            VALUES (NULL, '$login', '$tipo', '$nombre', '$password', '$destino')");
        return $query;
    }

    public function deleteUsuario($idUsuario){
        $query = $this->con->query("DELETE FROM usuarios WHERE id_usu=$idUsuario");
        return $query;
    }

    public function updateUsuario($idUsuario,$nombre, $tipo, $login, $password){
        $query = $this->con->query("UPDATE  `usuarios` SET  `login` =  '$login', 
                                                                   `tipo` =  '$tipo', 
                                                                    `nombre` =  '$nombre', 
                                                                    `password` =  '$password' ,`foto` =  'fotoUsuario/user.png' WHERE  `usuarios`.`id_usu` ='$idUsuario'");
        return $query;
    }


    public function updateDataFactura($iddatos, $propietario, $razon, $direccion, $nro,$telefono){
        $query = $this->con->query("UPDATE  `datos` SET  `propietario` =  '$propietario', 
                                                                `razon` =  '$razon', 
                                                                `direccion` =  '$direccion', 
                                                                `telefono` =  '$telefono', 
                                                                `nro` =  '$nro'    WHERE  `datos`.`iddatos` ='$iddatos'");

        return $query;
    }

    public function deleteProveedor($idProveedor){
        $query = $this->con->query("DELETE FROM proveedor WHERE idproveedor=$idProveedor");
        return $query;
    }

    public function registerNewProveedor($proveedor, $responsable, $direccion, $telefono, $fechaRegistro){

        $query = $this->con->query( "INSERT INTO `proveedor` (`idproveedor`, `proveedor`, `responsable`, `fechaRegistro`, `direccion`, `telefono`, `estado`, `fechaAviso`, `valor`, `valorCobrado`, `saldo`)
        VALUES (NULL, '$proveedor', '$responsable', '$fechaRegistro', '$direccion', '$telefono', 'Registro', '2020-05-14', '100', '50', 'Saldo')");
        return $query;
    }

    public function editProveedor($proveedorId,$proveedor, $responsable, $direccion, $telefono, $fechaRegistro){

        $query = $this->con->query( "UPDATE `proveedor` SET `proveedor` = '$proveedor', 
                                                                   `responsable` = '$responsable', 
                                                                   `fechaRegistro` = '$fechaRegistro', 
                                                                   `direccion` = '$direccion', 
                                                                   `telefono` = '$telefono'
                                                                    WHERE `proveedor`.`idproveedor` = $proveedorId");
        return $query;
    }




}

?>