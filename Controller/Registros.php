<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$usuarioLogin = $_POST['usuarioLogin'];
$passwordLogin = $_POST['passwordLogin'];

$con = new conexion();

$allUsuarios = $con->getAllUsersData();
$menuMain = $con->getMenu();

if(isset($_POST['a_nuevo'])){
    $usuario=$_POST['login'];
    $tipo=$_POST['tipo'];
    $nombre=$_POST['nombre'];
    $password=$_POST['password'];
    $destino ='fotoUsuario/user.png';

    $mensaje="Se Añadio un nuevo Usuario";
    $alerta="alert alert-success";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $registerNewUser = $con->registerNewUser($nombre, $tipo, $usuario, $password, $destino);

}

if(isset($_GET['idborrar'])){
    $idUsuario=$_GET['idborrar'];
    $usuarioLogin=$_GET['usuarioLogin'];
    $passwordLogin=$_GET['passwordLogin'];

    $mensaje="Se Elimino un Usuario";
    $alerta="alert alert-danger";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $deleteUser = $con->deleteUsuario($idUsuario);

}

if(isset($_POST['update_data'])){
    $idUsuarioData=$_POST['idUsuario'];
    $login=$_POST['login'];
    $tipo=$_POST['tipo'];
    $nombre=$_POST['nombre'];
    $password=$_POST['password'];

    $usuarioLogin=$_POST['usuarioLogin'];
    $passwordLogin=$_POST['passwordLogin'];

    $mensaje="Se Actualizo los datos del Usuario";
    $alerta="alert alert-info";

    $updateMensaje = $con->updateMensajeAlerta($mensaje);
    $updateAlerta = $con->updateAlerta($alerta);

    $updateUser = $con->updateUsuario($idUsuarioData, $nombre, $tipo, $login, $password);

}

header("Location: Usuario.php?usuario=$usuarioLogin&password=$passwordLogin&estado='Activo'");

?>