<?php

require('../Model/conexion.php');

if (!isset($_SESSION)) {
    session_start();
}
$con = new conexion();
$dataFacturaTiquet = $con->getDataFacturaTiquet();
foreach ($dataFacturaTiquet as $tiquet) {
    $usuario = $tiquet['usuario'];
    $contrasena = $tiquet['password'];
}
$showPreVenta = $con->getPreVenta();

$userRegister = $con->getUser($usuario, $contrasena);

foreach ($userRegister as $user) {
    $tipo = $user['tipo'];
    $id_usuario = $user['id_usu'];
    $nombresUsuario = $user['nombre'];
    $foto = $user['foto'];
    $password= $user['password'];
    $login= $user['login'];
}

$codigoControl=$con->getCodigoControl();
foreach ($codigoControl as $codigo) {
    $codTransaccion = $codigo['codigoTransaccion'];
}

 while($datosPreventa=mysqli_fetch_array($showPreVenta)){

            $idpreventa=$datosPreventa['idpreventa'];
            $imagen=$datosPreventa['imagen'];
            $producto=$datosPreventa['producto'];
            $cantidad=$datosPreventa['cantidad'];
            $totalPrecio=$datosPreventa['totalPrecio'];
            $idproducto=$datosPreventa['idproducto'];
            $pventa=$datosPreventa['pventa'];
            $idUser=$datosPreventa['idUser'];
            $iprecio=$datosPreventa['precio'];
            $tipo=$datosPreventa['tipo'];

      $fechaActualRegistro=date("Y-m-d");
      $nombre=$nombresUsuario;
      $estadoTransaccion="No Consolidado";

      $registerPreventa = $con->registerPreventaDetalle($fechaActualRegistro, $codTransaccion, $nombre, $cantidad, $producto, $totalPrecio, ($cantidad*$totalPrecio), $tipo, $idUser, $estadoTransaccion);

  }


$showPreVentaByPerson = $con->getPreVentaTotalByPerson();
while($datosPreventaByPerson=mysqli_fetch_array($showPreVentaByPerson)){

    $totalPrecio=$datosPreventaByPerson['totalPrecio'];
    $idUser=$datosPreventaByPerson['idUser'];

    $fechaActualRegistro=date("Y-m-d");
    $nombre=$nombresUsuario;
    $estadoTransaccion="No Consolidado";

    $registerPreventa = $con->registerPreventaDetalleTotal($fechaActualRegistro, $codTransaccion, $nombre, $totalPrecio, $idUser, $estadoTransaccion);

 }

$cleanDataBasePreventa= $con->cleanRegisterPreventa();
$cleanCodigoControl= $con->cleanCodigoControl();
$cleanClienteData = $con->cleanClienteData();
$cleanDataFactura = $con->cleanDataFactura();

header("Location: Ventas.php?usuario=$login&password=$password");


?>