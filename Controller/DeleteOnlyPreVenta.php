<?php
require('../Model/conexion.php');


if (!isset($_SESSION)) {
    session_start();
}

$idproducto = $_GET['idproducto'];

$con = new conexion();

$getUserSession = $con->getUserSession();

foreach ($getUserSession as $userSession) {
    $usuario= $userSession['usuario'];
    $password = $userSession['password'];
}

$deleteOnlyPreventa = $con->deleteUnaPreVenta($idproducto);

$showPreVenta = $con->getPreVenta();
$totalPreVenta = $con->getTotalPreVenta();

//require('../Views/pedido.php');

header("Location: Ventas.php?usuario=$usuario&password=$password");

?>